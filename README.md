# Publishing to MavenCentral

## User Management

User changes müssen über [https://issues.sonatype.org/](https://issues.sonatype.org/) requested werden.

## MavenCentral Releases

Neue Artifakte werden in einem 2-stufigen Prozess auf MavenCentral veröffentlicht. Builds werden mit dem Gradle-Task `publishToSonatype` auf [https://oss.sonatype.org/](https://oss.sonatype.org/) hochgeladen.
Nur Versionen ohne `-SNAPSHOT`-Suffix können auf MavenCentral released werden. SNAPSHOT-Versionen werden im SNAPSHOT-Repository veröffentlicht, während Versionen ohne Suffix im Release-Repo von sonatype erscheinen und von da dann released werden können.
Für den Upload mit Gradle sind folgende Angaben im `gradle.properties` nötig:

```
signing.keyId=<Die GPG Key-Id des Users>
signing.password=<Die GPG passphrase für den Key>
signing.secretKeyRingFile=<Der Pfad zum GPG Key-Ring-File zB /Users/tinel/.gnupg/secring.kbx>
sonatypeUsername=<User-Name des Access-Tokens für Sonytpe. Kann auf oss.sonatype.org erstellt werden>
sonatypePassword=<Passwort zum jeweiligen Access-Tokens für den Sonatype-User>
```

In einem nächsten Schritt kann der jeweilige Artifakt direkt über [https://oss.sonatype.org/](https://oss.sonatype.org/) final released werden.
