package ch.approppo.signingplugin

import ch.approppo.signingplugin.config.SigningConfigReader
import ch.approppo.signingplugin.config.SigningPluginConfig
import ch.approppo.signingplugin.config.SigningPluginDescriptionTask
import ch.approppo.signingplugin.models.CredentialConfig
import ch.approppo.signingplugin.providers.ExternalFolderSigningLocationProvider
import ch.approppo.signingplugin.providers.IndividualProjectSigningLocationProvider
import ch.approppo.signingplugin.providers.GradlePropertiesCredentialsProvider
import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 *
 */
class SigningPlugin implements Plugin<Project> {


    void apply(Project project) {
        project.logger.lifecycle("Run './gradlew describeSigningPlugin' to get more info how to use the signing plugin or use './gradlew --info | SigningPlugin:' to get more info during the configuration phase")
        project.tasks.register("describeSigningPlugin", SigningPluginDescriptionTask)

        final SigningConfigReader configReader = new SigningConfigReader(project)
        final SigningPluginConfig config = configReader.readConfig()

        final CredentialConfigResolver resolver = new CredentialConfigResolver(config)
        resolver.registerProvider(new GradlePropertiesCredentialsProvider(project))
        resolver.registerProvider(new IndividualProjectSigningLocationProvider(project))
        resolver.registerProvider(new ExternalFolderSigningLocationProvider(project))

        final CredentialConfig resolvedConfig = resolver.resolve()

        final SigningCredentialsApplier applier = new SigningCredentialsApplier(project, config)
        applier.applyCredentialConfig(resolvedConfig)
    }
}
