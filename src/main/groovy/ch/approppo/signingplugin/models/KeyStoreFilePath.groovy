package ch.approppo.signingplugin.models
/**
 *
 *
 * @author Martin Neff, approppo GmbH (martin.neff@approppo.ch)
 * @since 15.04.22
 */
class KeyStoreFilePath extends CredentialItem {

    private static final PROPERTY_KEY = "KEYSTORE_NAME"
    private static final PROJECT_PROPERTY_NAME = "storeFilePath"

    KeyStoreFilePath(String prefix) {
        super(prefix)
    }

    @Override
    protected String getUnPrefixedProjectPropertyName() {
        return PROJECT_PROPERTY_NAME
    }

    @Override
    protected String getUnPrefixedPropertyKey() {
        return PROPERTY_KEY
    }

    @Override
    boolean isValid() {
        return super.isValid() && new File(getPropertyValue()).exists()
    }
}
