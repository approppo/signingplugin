package ch.approppo.signingplugin.models


/**
 *
 *
 * @author Martin Neff, approppo GmbH (martin.neff@approppo.ch)
 * @since 15.04.22
 */
class StorePassword extends CredentialItem {

    private static final PROPERTY_KEY = "KEYSTORE_PW"
    private static final PROJECT_PROPERTY_NAME = "storePassword"

    StorePassword(String prefix) {
        super(prefix)
    }

    @Override
    protected String getUnPrefixedProjectPropertyName() {
        return PROJECT_PROPERTY_NAME
    }

    @Override
    protected String getUnPrefixedPropertyKey() {
        return PROPERTY_KEY
    }
}
