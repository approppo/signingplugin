package ch.approppo.signingplugin.models


/**
 *
 *
 * @author Martin Neff, approppo GmbH (martin.neff@approppo.ch)
 * @since 15.04.22
 */
class KeyAlias extends CredentialItem {

    private static final PROPERTY_KEY = "KEY_ALIAS"
    private static final PROJECT_PROPERTY_NAME = "keyAlias"

    KeyAlias(String prefix) {
        super(prefix)
    }

    @Override
    protected String getUnPrefixedProjectPropertyName() {
        return PROJECT_PROPERTY_NAME
    }

    @Override
    protected String getUnPrefixedPropertyKey() {
        return PROPERTY_KEY
    }
}
