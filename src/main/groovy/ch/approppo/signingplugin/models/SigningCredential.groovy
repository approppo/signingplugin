package ch.approppo.signingplugin.models


/**
 *
 *
 * @author Martin Neff, approppo GmbH (martin.neff@approppo.ch)
 * @since 15.04.22
 */
class SigningCredential {

    private final String env
    private final KeyStoreFilePath keyStoreFilePath
    private final StorePassword storePassword
    private final KeyAlias keyAlias
    private final KeyPassword keyPassword

    SigningCredential(String env, KeyStoreFilePath keyStoreFilePath, StorePassword storePassword, KeyAlias keyAlias, KeyPassword keyPassword) {
        this.env = env
        this.keyStoreFilePath = keyStoreFilePath
        this.storePassword = storePassword
        this.keyAlias = keyAlias
        this.keyPassword = keyPassword
    }

    String getEnv() {
        return env
    }

    KeyStoreFilePath getKeyStoreFilePath() {
        return keyStoreFilePath
    }

    StorePassword getStorePassword() {
        return storePassword
    }

    KeyAlias getKeyAlias() {
        return keyAlias
    }

    KeyPassword getKeyPassword() {
        return keyPassword
    }

    boolean isValid() {
        return keyStoreFilePath.isValid()
                && storePassword.isValid()
                && keyAlias.isValid()
                && keyPassword.isValid()
    }

    void resetValuesToEmpty() {
        keyStoreFilePath.resetValueToEmpty()
        storePassword.resetValueToEmpty()
        keyAlias.resetValueToEmpty()
        keyPassword.resetValueToEmpty()
    }

    @Override
    String toString() {
        return "keyStoreFilePath: ${keyStoreFilePath.propertyKey}, " +
                "storePassword: ${storePassword.propertyKey}, " +
                "keyAlias: ${keyAlias.propertyKey}, " +
                "keyPassword: ${keyPassword.propertyKey}"
    }
}
