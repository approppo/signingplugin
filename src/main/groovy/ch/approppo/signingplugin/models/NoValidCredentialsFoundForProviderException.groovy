package ch.approppo.signingplugin.models


/**
 *
 *
 * @author Martin Neff, approppo GmbH (martin.neff@approppo.ch)
 * @since 16.04.22
 */
class NoValidCredentialsFoundForProviderException extends Exception {}
