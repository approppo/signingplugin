package ch.approppo.signingplugin.models

/**
 *
 *
 * @author Martin Neff, approppo GmbH (martin.neff@approppo.ch)
 * @since 07.08.18
 */
class CredentialConfig {

    private final String credentialSource
    private final List<SigningCredential> signingCredentials

    CredentialConfig(String credentialSource, List<SigningCredential> signingCredentials) {
        this.credentialSource = credentialSource
        this.signingCredentials = signingCredentials
    }

    boolean isConfigValid() {
        return signingCredentials.every { it.isValid() }
    }

    String getCredentialSource() {
        return credentialSource
    }

    List<SigningCredential> getSigningCredentials() {
        return signingCredentials
    }

    String getLoggingString() {
        if (this instanceof EmptyCredentialConfig) {
            return "No credentials found in different sources. Applying empty credentials for ${getEnvs()}"
        } else {
            return "Found credentials in ${getCredentialSource()}, applying credentials for ${getEnvs()}"
        }
    }

    @Override
    String toString() {
        return "Credential source: ${credentialSource}, valid: ${isConfigValid()}"
    }

    private String getEnvs() {
        return signingCredentials.collect { it.getEnv() }.toListString()
    }
}
