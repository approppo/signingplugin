package ch.approppo.signingplugin.models


/**
 *
 *
 * @author Martin Neff, approppo GmbH (martin.neff@approppo.ch)
 * @since 15.04.22
 */
abstract class CredentialItem {

    protected final String prefixedPropertyKey
    protected final String prefixedProjectPropertyName
    private String propertyValue = ""

    CredentialItem(String propertyKeyPrefix) {
        String unPrefixedPropertyKey = getUnPrefixedPropertyKey()
        String unPrefixedProjectPropertyName = getUnPrefixedProjectPropertyName()
        if (propertyKeyPrefix == null || propertyKeyPrefix.isBlank()) {
            this.prefixedPropertyKey = unPrefixedPropertyKey
            this.prefixedProjectPropertyName = unPrefixedProjectPropertyName
        } else {
            this.prefixedPropertyKey = propertyKeyPrefix + "_" + unPrefixedPropertyKey
            this.prefixedProjectPropertyName = propertyKeyPrefix + "_" + unPrefixedProjectPropertyName
        }
    }

    abstract protected String getUnPrefixedProjectPropertyName()

    abstract protected String getUnPrefixedPropertyKey()

    String getPropertyKey() {
        return prefixedPropertyKey
    }

    String getProjectPropertyName() {
        return prefixedProjectPropertyName
    }

    void setPropertyValue(String value) {
        this.propertyValue = value
    }

    String getPropertyValue() {
        return propertyValue
    }

    boolean isValid() {
        return propertyValue != null && !propertyValue.isBlank()
    }

    void resetValueToEmpty() {
        propertyValue = ""
    }
}
