package ch.approppo.signingplugin.models


/**
 *
 *
 * @author Martin Neff, approppo GmbH (martin.neff@approppo.ch)
 * @since 15.04.22
 */
class EmptyCredentialConfig extends CredentialConfig {

    EmptyCredentialConfig(List<SigningCredential> signingCredentials) {
        super("Empty fallback credentials", signingCredentials)
        signingCredentials.each { it.resetValuesToEmpty() }
    }

    @Override
    boolean isConfigValid() {
        return true
    }
}
