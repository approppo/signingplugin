package ch.approppo.signingplugin

import ch.approppo.signingplugin.config.SigningPluginConfig
import ch.approppo.signingplugin.models.CredentialConfig
import ch.approppo.signingplugin.models.EmptyCredentialConfig
import ch.approppo.signingplugin.models.SigningCredential
import ch.approppo.signingplugin.providers.SigningCredentialProvider

/**
 * @author Martin Neff, approppo GmbH (martin.neff@approppo.ch)
 * @since 15.04.22
 */
class CredentialConfigResolver {

    private final SigningPluginConfig signingPluginConfig
    private final List<SigningCredential> signingCredentials
    private final List<SigningCredentialProvider> providers = new ArrayList<>()

    CredentialConfigResolver(SigningPluginConfig signingPluginConfig) {
        this.signingPluginConfig = signingPluginConfig
        this.signingCredentials = signingPluginConfig.createSigningCredentials()
    }

    void registerProvider(SigningCredentialProvider provider) {
        providers.add(provider)
    }

    CredentialConfig resolve() {
        def iterator = providers.iterator()
        while (iterator.hasNext()) {
            def provider = iterator.next()
            CredentialConfig credentialConfig = provider.readSigningConfigs(signingCredentials)
            if (credentialConfig.isConfigValid()) {
                return credentialConfig
            } else {
                provider.logHintForProvider()
            }
        }

        return new EmptyCredentialConfig(signingCredentials)
    }
}
