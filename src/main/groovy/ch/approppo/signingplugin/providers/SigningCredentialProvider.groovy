package ch.approppo.signingplugin.providers

import ch.approppo.signingplugin.models.CredentialConfig
import ch.approppo.signingplugin.models.SigningCredential
import org.gradle.api.Project

/**
 * @author Martin Neff, approppo GmbH (martin.neff@approppo.ch)
 * @since 17.02.17
 */
abstract class SigningCredentialProvider {

    protected final Project project

    SigningCredentialProvider(Project project) {
        this.project = project
    }

    protected void logStartRead() {
        project.logger.info("SigningPlugin: Trying to read signing credentials from ${getSource()}...")
    }

    protected static void loadCredentialsFromProperties(Map<String, ?> properties, List<SigningCredential> signingCredentials, String basePath = null) {
        signingCredentials.each {
            String keyStoreFilePath = properties[it.keyStoreFilePath.propertyKey]
            if (basePath != null) {
                keyStoreFilePath = new File(basePath, keyStoreFilePath).absolutePath
            }
            it.keyStoreFilePath.propertyValue = keyStoreFilePath
            it.keyAlias.propertyValue = properties[it.keyAlias.propertyKey]
            it.keyPassword.propertyValue = properties[it.keyPassword.propertyKey]
            it.storePassword.propertyValue = properties[it.storePassword.propertyKey]
        }
    }

    abstract CredentialConfig readSigningConfigs(List<SigningCredential> signingCredentials)

    abstract void logHintForProvider()

    abstract String getSource()
}
