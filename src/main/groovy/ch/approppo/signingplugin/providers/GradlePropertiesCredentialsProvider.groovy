package ch.approppo.signingplugin.providers

import ch.approppo.signingplugin.models.CredentialConfig
import ch.approppo.signingplugin.models.SigningCredential
import org.gradle.api.Project

/**
 * @author Martin Neff, approppo GmbH (martin.neff@approppo.ch)
 * @since 17.02.17
 */
class GradlePropertiesCredentialsProvider extends SigningCredentialProvider {

    GradlePropertiesCredentialsProvider(Project project) {
        super(project)
    }

    @Override
    CredentialConfig readSigningConfigs(List<SigningCredential> signingCredentials) {
        logStartRead()
        loadCredentialsFromProperties(project.properties, signingCredentials)
        return new CredentialConfig(getSource(), signingCredentials)
    }

    @Override
    void logHintForProvider() {
        project.logger.info("SigningPlugin: ...There were no properties in the project found (e.g. KEYSTORE_NAME). You could add the required properties to a gradle properties file and apply it to the build script.")
    }

    @Override
    String getSource() {
        return "gradle properties"
    }
}

