package ch.approppo.signingplugin.providers

import ch.approppo.signingplugin.models.CredentialConfig
import ch.approppo.signingplugin.models.SigningCredential
import org.gradle.api.Project

/**
 *
 *
 * @author Martin Neff, approppo GmbH (martin.neff@approppo.ch)
 * @since 15.04.22
 */
class IndividualProjectSigningLocationProvider extends FolderBasedProvider {

    public static final String EXTERNAL_PROJECT_FOLDER_PATH = 'EXTERNAL_PROJECT_FOLDER_PATH'

    IndividualProjectSigningLocationProvider(Project project) {
        super(project)
    }

    @Override
    CredentialConfig readSigningConfigs(List<SigningCredential> signingCredentials) {
        logStartRead()
        if (project.hasProperty(EXTERNAL_PROJECT_FOLDER_PATH)) {
            String folderPath = project.property(EXTERNAL_PROJECT_FOLDER_PATH) as String
            File signingConfigFile = new File(folderPath, DEFAULT_SIGNING_CONFIG_FILE_NAME)
            if (signingConfigFile.exists()) {
                Properties properties = loadPropertiesFromFile(signingConfigFile)
                loadCredentialsFromProperties(properties, signingCredentials, folderPath)
            }
        }
        return new CredentialConfig(getSource(), signingCredentials)
    }

    @Override
    void logHintForProvider() {
        project.logger.info("SigningPlugin: ...There was no property EXTERNAL_PROJECT_FOLDER_PATH found which points to a folder containing a android_signing.properties with all the required credentials. Add the property EXTERNAL_PROJECT_FOLDER_PATH to your gradle.properties file")
    }

    @Override
    String getSource() {
        return "individual project signing location"
    }
}
