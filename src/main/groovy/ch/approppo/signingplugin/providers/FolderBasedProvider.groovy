package ch.approppo.signingplugin.providers


import org.gradle.api.Project

/**
 *
 *
 * @author Martin Neff, approppo GmbH (martin.neff@approppo.ch)
 * @since 15.04.22
 */
abstract class FolderBasedProvider extends SigningCredentialProvider {

    public static final String DEFAULT_SIGNING_CONFIG_FILE_NAME = "android_signing.properties"

    FolderBasedProvider(Project project) {
        super(project)
    }

    protected static Properties loadPropertiesFromFile(File propertiesFile) {
        Properties credentialsProperties = new Properties()
        credentialsProperties.load(new FileInputStream(propertiesFile))
        return credentialsProperties
    }
}
