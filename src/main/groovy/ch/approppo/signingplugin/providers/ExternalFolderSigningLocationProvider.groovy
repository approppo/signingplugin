package ch.approppo.signingplugin.providers


import ch.approppo.signingplugin.models.CredentialConfig
import ch.approppo.signingplugin.models.SigningCredential
import org.gradle.api.Project

/**
 * @author Martin Neff, approppo GmbH (martin.neff@approppo.ch)
 * @since 17.02.17
 */
class ExternalFolderSigningLocationProvider extends FolderBasedProvider {

    public static final String EXTERNAL_FOLDER_PATH = 'EXTERNAL_FOLDER_PATH'

    ExternalFolderSigningLocationProvider(Project project) {
        super(project)
    }

    @Override
    CredentialConfig readSigningConfigs(List<SigningCredential> signingCredentials) {
        logStartRead()
        if (project.hasProperty(EXTERNAL_FOLDER_PATH)) {
            String folderPath = project.property(EXTERNAL_FOLDER_PATH) as String
            String projectName = project.rootProject.name
            String basePath = new File(folderPath, projectName).absolutePath
            File signingConfigFile = new File(basePath, DEFAULT_SIGNING_CONFIG_FILE_NAME)
            if (signingConfigFile.exists()) {
                Properties properties = loadPropertiesFromFile(signingConfigFile)
                loadCredentialsFromProperties(properties, signingCredentials, basePath)
            }
        }
        return new CredentialConfig(getSource(), signingCredentials)
    }

    @Override
    void logHintForProvider() {
        project.logger.info("SigningPlugin: ...There was no property EXTERNAL_FOLDER_PATH found which points to a root folder, containing a folder with the same name as the rootProject defined in the settings.gradle file. Add such a property to the gradle.properties file or to .gradle/gradle.properties in your home directory.")
    }

    @Override
    String getSource() {
        return "external folder"
    }
}

