package ch.approppo.signingplugin

import ch.approppo.signingplugin.config.SigningPluginConfig
import ch.approppo.signingplugin.models.CredentialConfig
import ch.approppo.signingplugin.models.CredentialItem
import org.gradle.api.Project

/**
 *
 *
 * @author Martin Neff, approppo GmbH (martin.neff@approppo.ch)
 * @since 07.08.18
 */
class SigningCredentialsApplier {

    private final Project project
    private final SigningPluginConfig signingPluginConfig

    SigningCredentialsApplier(Project project, SigningPluginConfig signingPluginConfig) {
        this.signingPluginConfig = signingPluginConfig
        this.project = project
    }

    void applyCredentialConfig(CredentialConfig credentialConfig) {
        project.logger.lifecycle(credentialConfig.getLoggingString())
        def signingCredentials = credentialConfig.getSigningCredentials()
        signingCredentials.each {
            applyCredentialItem(it.keyStoreFilePath)
            applyCredentialItem(it.storePassword)
            applyCredentialItem(it.keyAlias)
            applyCredentialItem(it.keyPassword)
        }
    }

    private void applyCredentialItem(CredentialItem credentialItem) {
        String key = credentialItem.getProjectPropertyName()
        String value = credentialItem.getPropertyValue()
        project.ext.set(key, value)
    }
}
