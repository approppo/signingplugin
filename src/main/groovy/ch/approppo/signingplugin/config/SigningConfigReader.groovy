package ch.approppo.signingplugin.config

import org.gradle.api.Project

/**
 *
 *
 * @author Martin Neff, approppo GmbH (martin.neff@approppo.ch)
 * @since 15.04.22
 */
class SigningConfigReader {

    private final Project project

    SigningConfigReader(Project project) {
        this.project = project
    }

    SigningPluginConfig readConfig() {
        List<String> envs = new ArrayList<>()
        if (project.hasProperty("signingEnvs")) {
            envs.addAll(project.properties["signingEnvs"] as ArrayList<String>)
        }

        if (envs.isEmpty()) {
            envs.add("")
            project.logger.info("SigningPlugin: No envs found in the build script, using property keys without prefix (referred to as DEFAULT). If you wish to have different prefixed keys, then use 'project.ext.signingEnvs = ['ENV1', 'ENV2']' with the required envs in your build script")
        } else {
            project.logger.info("SigningPlugin: Found the following envs in the build script: ${envs.toListString()}")
        }

        return new SigningPluginConfig(envs)
    }
}
