package ch.approppo.signingplugin.config

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction


/**
 *
 *
 * @author Martin Neff, approppo GmbH (martin.neff@approppo.ch)
 * @since 14.04.22
 *
 */
abstract class SigningPluginDescriptionTask extends DefaultTask {

    @TaskAction
    def printDescription() {
        project.logger.lifecycle("-------------------------------------------------------------------------------------")
        project.logger.lifecycle("----------- Description of the signing plugin version ${project.version}: -----------")
        project.logger.lifecycle("")
        project.logger.lifecycle("This plugin checks for signing infos in the gradle properties or as an alternative in an external file storage.")
        project.logger.lifecycle("")
        project.logger.lifecycle("List of properties required in the gradle properties or in the external storage:")
        project.logger.lifecycle("Property key:     Property value")
        project.logger.lifecycle("-------------     ----------------------------------------")
        project.logger.lifecycle("KEYSTORE_NAME:    path to the keystore file")
        project.logger.lifecycle("KEYSTORE_PW:      password to the keystore file")
        project.logger.lifecycle("KEY_ALIAS:        key alias in the keystore file")
        project.logger.lifecycle("KEY_PW:           password to the key in the keystore file")
        project.logger.lifecycle("----------------------------------------------------------")
        project.logger.lifecycle("")
        project.logger.lifecycle("The set of properties can be prefixed with a String which has to be registered in the build.gradle:")
        project.logger.lifecycle("E.g.: project.ext.signingEnvs = ['INTERNAL', 'UPLOAD']")
        project.logger.lifecycle("")
        project.logger.lifecycle("Possible locations for signing configs:")
        project.logger.lifecycle("--- Gradle properties: <some-name>.properties ---")
        project.logger.lifecycle("Add the above mentioned with the desired (optional) prefixes to a properties file for gradle and apply it to your build script")
        project.logger.lifecycle("-------------------------------------------------")
        project.logger.lifecycle("")
        project.logger.lifecycle("--- Config in external folder: ---")
        project.logger.lifecycle("Put your keystore file in a folder outside of your project. Add a properties file with the name 'android_signing.properties'.")
        project.logger.lifecycle("The structure of the properties file has to follow to before mentioned property naming.")
        project.logger.lifecycle("")
        project.logger.lifecycle("Project specific definition:")
        project.logger.lifecycle("Add a property to your gradle.properties file named EXTERNAL_PROJECT_FOLDER_PATH that points to the folder with the keystore and the properties file")
        project.logger.lifecycle("")
        project.logger.lifecycle("Global definition:")
        project.logger.lifecycle("Add a property to your global gradle.properties file named EXTERNAL_FOLDER_PATH points to a folder where the project specific keystores and property files are.:")
        project.logger.lifecycle("_ external folder")
        project.logger.lifecycle(" |_ project name (rootName in settings.gradle)")
        project.logger.lifecycle("   |_ keystore")
        project.logger.lifecycle("   |_ android_signing.properties")
        project.logger.lifecycle("-------------------------------------------------")
        project.logger.lifecycle("")
        project.logger.lifecycle("-------------------------------------------------------------------------------------")
        project.logger.lifecycle("-------------------------------------------------------------------------------------")
    }
}
