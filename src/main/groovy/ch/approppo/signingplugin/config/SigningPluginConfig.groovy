package ch.approppo.signingplugin.config

import ch.approppo.signingplugin.models.*

/**
 *
 *
 * @author Martin Neff, approppo GmbH (martin.neff@approppo.ch)
 * @since 2019-02-11
 */

class SigningPluginConfig {

    private final List<String> signingEnvs

    SigningPluginConfig(List<String> signingEnvs) {
        this.signingEnvs = signingEnvs
    }

    List<SigningCredential> createSigningCredentials() {
        List<SigningCredential> signingCredentials = new ArrayList<>()
        signingEnvs.each {
            String env
            if (it.isBlank()) {
                env = "DEFAULT"
            } else {
                env = it
            }
            signingCredentials.add(
                    new SigningCredential(
                            env,
                            new KeyStoreFilePath(it),
                            new StorePassword(it),
                            new KeyAlias(it),
                            new KeyPassword(it)
                    )
            )
        }
        return signingCredentials
    }

    @Override
    String toString() {
        return signingEnvs.toListString()
    }
}
